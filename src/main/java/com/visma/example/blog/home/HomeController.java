package com.visma.example.blog.home;

import com.visma.example.blog.post.Post;
import com.visma.example.blog.post.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private PostService postService;

    @ModelAttribute("allPosts")
    public List<Post> allPosts() {
        return postService.findAllPosts();
    }

    @RequestMapping("/")
    public String home() {
        return "index";
    }
}
