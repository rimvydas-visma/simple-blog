package com.visma.example.blog.post;

import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class InMemoryPostRepository implements PostRepository {
    private Map<UUID, Post> posts = new HashMap<>();

    @Override
    public List<Post> findAll() {
        return posts.values()
                .stream()
                .sorted((p1, p2) -> p2.getCreatedTime().compareTo(p1.getCreatedTime()))
                .collect(Collectors.toList());
    }

    @Override
    public Post save(Post post) {
        if (post.getId() == null) {
            post.setId(UUID.randomUUID());
        }
        posts.put(post.getId(), post);
        return post;
    }

    @Override
    public Post get(UUID id) {
        return posts.get(id);
    }
}
