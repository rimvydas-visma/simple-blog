package com.visma.example.blog.post;

import java.util.List;
import java.util.UUID;

public interface PostRepository {
    List<Post> findAll();

    Post save(Post post);

    Post get(UUID id);
}
