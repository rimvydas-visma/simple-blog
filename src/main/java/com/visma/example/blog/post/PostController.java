package com.visma.example.blog.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class PostController {

    @Autowired
    private PostService postService;

    @RequestMapping("/post/{id}")
    public String viewPost(@PathVariable UUID id, Model model) {
        model.addAttribute("post", postService.getPost(id));
        return "post/view";
    }

    @RequestMapping(value = "/post/new", method = RequestMethod.GET)
    public String newPostPage(Post post) {
        return "post/edit";
    }

    @RequestMapping(value = "/post/new", method = RequestMethod.POST)
    public String saveNewPost(@Valid Post post, BindingResult result) {
        if (result.hasErrors()) {
            return "post/edit";
        }
        postService.saveNewPost(post);
        return "redirect:/";
    }
}
