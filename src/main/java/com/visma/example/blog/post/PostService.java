package com.visma.example.blog.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public List<Post> findAllPosts() {
        return postRepository.findAll();
    }

    public Post saveNewPost(Post post) {
        post.setId(null);
        post.setCreatedTime(LocalDateTime.now());
        return postRepository.save(post);
    }

    public Post getPost(UUID id) {
        return postRepository.get(id);
    }
}
